package app.findp.service;

import app.findp.entities.ActionEntity;
import app.findp.entities.utils.Util;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ActionService {

    String BASE_URL = "http://" + Util.ipAndPort + "/acs/actions/";

    // POST
    @POST(".")
    Call<Object> invokeAction(@Body ActionEntity actionEntity);
}
