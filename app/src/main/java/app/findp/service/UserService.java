package app.findp.service;

import app.findp.entities.UserEntity;
import app.findp.entities.utils.NewUserDetails;
import app.findp.entities.utils.Util;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserService {

    String BASE_URL = "http://" + Util.ipAndPort + "/acs/users/";

    // POST
    @POST(".")
    Call<UserEntity> createNewUser(@Body NewUserDetails newUserDetails);

    // PUT
    @PUT("{userDomain}/{userEmail}")
    Call<Void> updateUserDetails(@Path("userDomain") String userDomain, @Path("userEmail") String userEmail
        ,@Body UserEntity userEntity);

    // GET
    @GET("login/{userDomain}/{userEmail}")
    Call<UserEntity> login(@Path("userDomain") String userDomain, @Path("userEmail") String userEmail);
}
