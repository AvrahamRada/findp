package app.findp.service;

import app.findp.entities.ElementEntity;
import app.findp.entities.utils.ElementId;
import app.findp.entities.utils.Util;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ElementService {

    String BASE_URL = "http://" + Util.ipAndPort + "/acs/elements/";

    // POST
    @POST("{managerDomain}/{managerEmail}")
    Call<ElementEntity> createNewElement(@Path("managerDomain") String managerDomain, @Path("managerEmail") String managerEmail, @Body ElementEntity newElementEntity);

    // PUT
    @PUT("{managerDomain}/{managerEmail}/{elementDomain}/{elementId}")
    Call<Void> updateElement(@Path("managerDomain") String managerDomain, @Path("managerEmail") String managerEmail,
                             @Path("elementDomain") String elementDomain, @Path("elementId") String elementId,
                             @Body ElementEntity update);

    @PUT("{managerDomain}/{managerEmail}/{elementDomain}/{elementId}/children")
    Call<Void> bindParentElementToChildElement(@Path("managerDomain") String managerDomain, @Path("managerEmail") String managerEmail,
                                               @Path("elementDomain") String elementDomain,@Path("elementId") String elementId,
                                               @Body ElementId input);

    // GET
    @GET("{userDomain}/{userEmail}/{elementDomain}/{elementId}")
    Call<ElementEntity> getElement(@Path("userDomain") String userDomain, @Path("userEmail") String userEmail,
                                   @Path("elementDomain") String elementDomain, @Path("elementId") String elementId);

    @GET("{userDomain}/{userEmail}")
    Call<ElementEntity[]> getAllElements(@Path("userDomain") String userDomain, @Path("userEmail") String userEmail);

    @GET("{userDomain}/{userEmail}/{elementDomain}/{elementId}/children")
    Call<ElementEntity[]> getAllChildrenElements(@Path("userDomain") String userDomain, @Path("userEmail") String userEmail,
                                                 @Path("elementDomain") String elementDomain, @Path("elementId") String elementId);

    @GET("{userDomain}/{userEmail}/{elementDomain}/{elementId}/parents")
    Call<ElementEntity[]> getAllParentsElements(@Path("userDomain") String userDomain, @Path("userEmail") String userEmail,
                                                @Path("elementDomain") String elementDomain, @Path("elementId") String elementId);

    @GET("{userDomain}/{userEmail}/search/byName/{name}")
    Call<ElementEntity[]> getAllElementsByName(@Path("userDomain") String userDomain, @Path("userEmail") String userEmail, @Path("name") String name,
                                               @Query("size") int size, @Query("page") int page);

    @GET("{userDomain}/{userEmail}/search/byType/{type}")
    Call<ElementEntity[]> getAllElementsByType(@Path("userDomain") String userDomain, @Path("userEmail") String userEmail, @Path("name") String type,
                                               @Query("size") int size, @Query("page") int page);

    @GET("{userDomain}/{userEmail}/search/near/{lat}/{lng}/{distance}")
    Call<ElementEntity[]> getAllElementsByLocation(@Path("userDomain") String userDomain, @Path("userEmail")String userEmail,
                                                   @Path("lat") String lat, @Path("lng") String lng, @Path("distance") String distance,
                                                   @Query("size") int size, @Query("page") int page);

}
