package app.findp.data;


import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Comparator;

import app.findp.entities.utils.ElementId;
import app.findp.entities.utils.Location;

public class ParkingData implements Serializable {
    private String name;
    private Location location;
    private double distance;
    private boolean isTaken;
    private ElementId elementId;

    // constructor
    public ParkingData(String name, Location location,ElementId elementId,boolean isTaken) {
        this.name = name;
        this.location = location;
        this.isTaken = isTaken;
        this.elementId =elementId;
    }

    public void calculateDistance(Location userLocation) {
        double R = 6371; // Radius of the earth in km
        double dLat = deg2rad(userLocation.getLat()-location.getLat());  // deg2rad below
        double dLon = deg2rad(userLocation.getLng()-location.getLng());
        double a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                        Math.cos(deg2rad(location.getLat())) * Math.cos(deg2rad(userLocation.getLat())) *
                                Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c; // Distance in km
        DecimalFormat df = new DecimalFormat("#.##");
        distance = Double.parseDouble(df.format(d));
    }

    private double deg2rad(double deg) {
        return deg * (Math.PI/180);
    }



    //Getters and Setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public boolean isTaken() {
        return isTaken;
    }

    public void setTaken(boolean taken) {
        isTaken = taken;
    }

    public ElementId getElementId() {
        return elementId;
    }

    public void setElementId(ElementId elementId) {
        this.elementId = elementId;
    }

}
