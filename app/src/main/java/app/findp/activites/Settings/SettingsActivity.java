package app.findp.activites.Settings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.findp.R;
import app.findp.activites.Parking.MainActivity;
import app.findp.activites.Start.LoginActivity;
import app.findp.activites.Start.RegistrationActivity;
import app.findp.entities.UserEntity;
import app.findp.entities.utils.NewUserDetails;
import app.findp.entities.utils.Util;
import app.findp.service.UserService;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SettingsActivity extends AppCompatActivity {

    private TextInputLayout editText_userName;
    private ImageView back_btn;
    private Button update_btn;
    private UserService userService;
    private UserEntity userEntityToUpdate;
    // listView
    private ListView listView;
//    private String avatar;
//    private int[] image = {R.drawable.avatar01,R.drawable.avatar02,R.drawable.avatar03,R.drawable.avatar04,R.drawable.avatar05,R.drawable.avatar06};
//    private String[] headline = {"City employee","Student","Worker","Professor","Teacher","Anonymous"};

    private String avatar;
    private int[] image = {R.drawable.avatar01,R.drawable.avatar02,R.drawable.avatar03,R.drawable.avatar04,R.drawable.avatar05,R.drawable.avatar06};
    private String[] headline = {"Student","Worker","Anonymous","City employee","Professor","Teacher"};
    private String[] drawableName = {"avatar01","avatar02","avatar03","avatar04","avatar05","avatar06"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        init();
        setInfo();
        listeners();
    }

    private void init() {
        editText_userName = (TextInputLayout) findViewById(R.id.editText_userName);
        back_btn = (ImageView) findViewById(R.id.back_image);
        update_btn = (Button) findViewById(R.id.update_btn);

        userEntityToUpdate = (UserEntity) getIntent().getSerializableExtra(LoginActivity.USER);

        // static method
        OkHttpClient okHttpClient = Util.createOkHttpClient();
        Retrofit retrofit = Util.createRetrofit(okHttpClient,LoginActivity.USER);
        userService = retrofit.create(UserService.class);

        // ListView
        listView = findViewById(R.id.list_view);
        List<HashMap<String, String>> list = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("image", Integer.toString(image[i]));
            hashMap.put("headline", headline[i]);
            list.add(hashMap);
        }

        String[] from = {"image", "headline"};
        int[] to = {R.id.imageViewLogo, R.id.textViewHeadline1};

        SimpleAdapter simpleAdapter = new SimpleAdapter(getBaseContext(), list, R.layout.list_view_activity, from, to);
        listView.setAdapter(simpleAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                avatar = drawableName[position];
            }
        });


    }

    private void setInfo(){
        editText_userName.getEditText().setText(userEntityToUpdate.getUsername());
    }

    private void listeners() {
        back_btn.setOnClickListener(v -> {
            Intent mainActivityIntent = new Intent(SettingsActivity.this, MainActivity.class);
            mainActivityIntent.putExtra("user", userEntityToUpdate);
            startActivity(mainActivityIntent);
            finish();
        });
        update_btn.setOnClickListener(v -> {
            updateUser();
        });
    }


    private void updateUser(){
        Call<Void> callUser = userService.updateUserDetails(userEntityToUpdate.getUserId().getDomain(),userEntityToUpdate.getUserId().getEmail()
        ,new UserEntity(userEntityToUpdate.getUserId(),userEntityToUpdate.getRole(),editText_userName.getEditText().getText().toString().trim(),
                        avatar));
        callUser.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (!response.isSuccessful()) {
                    Log.i("TAG", "onResponse: " + response.code());
                    Toast.makeText(SettingsActivity.this, "Something wrong: " + response.toString(), Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    Log.i("TAG", "onResponse: " + response.code());
                    Toast.makeText(SettingsActivity.this, "Update Player User", Toast.LENGTH_SHORT).show();
                    userEntityToUpdate.setAvatar(avatar);
                    userEntityToUpdate.setUsername(editText_userName.getEditText().getText().toString().trim());
                    Intent mainActivityIntent = new Intent(SettingsActivity.this, MainActivity.class);
                    mainActivityIntent.putExtra(LoginActivity.USER, userEntityToUpdate);
                    startActivity(mainActivityIntent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(SettingsActivity.this, "Failure create User", Toast.LENGTH_SHORT).show();
                Log.i("TAG", "onFailure: " + t);
                return;
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent mainActivityIntent = new Intent(SettingsActivity.this, MainActivity.class);
        mainActivityIntent.putExtra("user", userEntityToUpdate);
        startActivity(mainActivityIntent);
        finish();
    }
}
