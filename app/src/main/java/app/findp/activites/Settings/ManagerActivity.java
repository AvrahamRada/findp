package app.findp.activites.Settings;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import java.util.HashMap;
import java.util.Map;

import app.findp.R;
import app.findp.activites.Parking.AddOrUpdateParkingActivity;
import app.findp.activites.Start.LoginActivity;
import app.findp.entities.ElementEntity;
import app.findp.entities.UserEntity;
import app.findp.entities.utils.ElementId;
import app.findp.entities.utils.Location;
import app.findp.entities.utils.Util;
import app.findp.service.ElementService;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ManagerActivity extends AppCompatActivity {

    public final static String ELEMENT = "element";
    public final static String PARK_ELEMENT_TO_UPDATE = "park element to update";
    public final static String USER = "user";
    private static final int SUCCESSFULLY_BACK = 1;
    private static final int SUCCESSFULLY_BACK2 = 2;
    private static final String ADD_PARKING = "ADD PARKING";
    private static final String EMPTY = "";

    private TextView managerView_name;
    private Button addParkingBtn;
    private Button updateParkingBtn;
    private Button editChoice;
    private ImageView logout_btn;

    ElementEntity[] children;
    private UserEntity managerUserEntity;
    private ElementEntity managerElementEntity;
    private ElementService elementService;
    private ElementEntity parkingElement;
    private ElementEntity parkToUpdate;
    private TableLayout mTableLayout;
    private Map<Integer,ElementId> elementId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager);
        init();
        getAllChildrenOfManager();
    }

    private void init() {
        // static method
        OkHttpClient okHttpClient = Util.createOkHttpClient();
        Retrofit retrofit = Util.createRetrofit(okHttpClient, ELEMENT);
        elementService = retrofit.create(ElementService.class);

        elementId = new HashMap<>();

        // Get USER MANAGER
        managerUserEntity = (UserEntity) getIntent().getSerializableExtra(USER);
        // Get ELEMENT MANAGER
        managerElementEntity = (ElementEntity) getIntent().getSerializableExtra(ELEMENT);
        managerView_name = (TextView) findViewById(R.id.manager_name);
        managerView_name.setText(EMPTY);
        managerView_name.setText(managerUserEntity.getUsername());
        parkingElement = new ElementEntity();
        parkToUpdate = new ElementEntity();

        logout_btn = (ImageView) findViewById(R.id.logout_btn);
        addParkingBtn = (Button) findViewById(R.id.add_parking);
        updateParkingBtn = (Button) findViewById(R.id.update_parking);
        updateParkingBtn.setTextColor(Color.parseColor("#1F000000"));
        editChoice = (Button) findViewById(R.id.edit_choice);
        editChoice.setVisibility(View.INVISIBLE);

        addParkingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Jump to ADD parking Activity
                Intent addParking = new Intent(ManagerActivity.this, AddOrUpdateParkingActivity.class);
                addParking.putExtra(USER, managerUserEntity);
                addParking.putExtra(ELEMENT, managerElementEntity);
                startActivityForResult(addParking, SUCCESSFULLY_BACK);
            }
        });

        updateParkingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Jump to UPDATE parking Activity
                Intent updateParking = new Intent(ManagerActivity.this, UpdateActivity.class);
                updateParking.putExtra(PARK_ELEMENT_TO_UPDATE, parkToUpdate);
                updateParking.putExtra(ELEMENT, managerElementEntity);
                updateParking.putExtra(USER, managerUserEntity);
                startActivityForResult(updateParking, SUCCESSFULLY_BACK2);
            }
        });

        editChoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateParkingBtn.setEnabled(false);
                updateParkingBtn.setTextColor(Color.parseColor("#1F000000"));
                arrangeAllChildrenInScrollView();
                v.setVisibility(View.INVISIBLE);
                mTableLayout.setEnabled(true);
                mTableLayout.setClickable(true);
            }
        });

        logout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case (SUCCESSFULLY_BACK): {
                if (resultCode == Activity.RESULT_OK) {
                    // Bring back CHILD that created
                    parkingElement = (ElementEntity) data.getSerializableExtra(ADD_PARKING);
                    Call<ElementEntity> createParkingElement = elementService.createNewElement(
                            managerUserEntity.getUserId().getDomain(),
                            managerUserEntity.getUserId().getEmail(),
                            parkingElement);

                    createParkingElement.enqueue(new Callback<ElementEntity>() {
                        @Override
                        public void onResponse(Call<ElementEntity> call, Response<ElementEntity> response) {
                            Log.i("TAG", "onResponse: " + response.code());
                            Toast.makeText(ManagerActivity.this, "*** Success with creating parking place ***", Toast.LENGTH_SHORT).show();

                            // bind parking place to City(Manager)
                            Call<Void> bindChildToParent = elementService.bindParentElementToChildElement(
                                    managerElementEntity.getElementId().getDomain(),
                                    managerUserEntity.getUserId().getEmail(),
                                    managerElementEntity.getElementId().getDomain(),
                                    managerElementEntity.getElementId().getId(),
                                    new ElementId(
                                            response.body().getElementId().getDomain(),
                                            response.body().getElementId().getId()));

                            bindChildToParent.enqueue(new Callback<Void>() {
                                @Override
                                public void onResponse(Call<Void> call, Response<Void> response) {
                                    Log.i("TAG", "onResponse: " + response.code());
                                    Toast.makeText(ManagerActivity.this, "*** Success on binding CHILD to PARENT ***", Toast.LENGTH_SHORT).show();
                                    getAllChildrenOfManager();
                                }

                                @Override
                                public void onFailure(Call<Void> call, Throwable t) {
                                    Toast.makeText(ManagerActivity.this, "*** Failure to binding CHILD to PARENT ***", Toast.LENGTH_SHORT).show();
                                    Log.i("TAG", "onFailure: " + t);
                                    return;
                                }
                            });
                        }

                        @Override
                        public void onFailure(Call<ElementEntity> call, Throwable t) {
                            Toast.makeText(ManagerActivity.this, "*** Failure with creating parking place ***", Toast.LENGTH_SHORT).show();
                            Log.i("TAG", "onFailure: " + t);
                            return;
                        }
                    });
                }
                break;
            }

            case SUCCESSFULLY_BACK2:{
                getAllChildrenOfManager();
                updateParkingBtn.setEnabled(false);
                updateParkingBtn.setTextColor(Color.parseColor("#1F000000"));
                editChoice.setVisibility(View.INVISIBLE);
                mTableLayout.setEnabled(true);
                mTableLayout.setClickable(true);
            }
        }
    }

    private void getAllChildrenOfManager() {
        // GET all children of Manager
        Call<ElementEntity[]> allChildrenElements = elementService.getAllChildrenElements(
                managerElementEntity.getElementId().getDomain(),
                managerUserEntity.getUserId().getEmail(),
                managerElementEntity.getElementId().getDomain(),
                managerElementEntity.getElementId().getId());

        allChildrenElements.enqueue(new Callback<ElementEntity[]>() {
            @Override
            public void onResponse(Call<ElementEntity[]> call, Response<ElementEntity[]> response) {
                Log.i("TAG", "onResponse: " + response.code());
                Toast.makeText(ManagerActivity.this, "Success on fetching all childrens of parent", Toast.LENGTH_SHORT).show();
                // GET ARRAY of all children of Manager
                children = response.body();
                arrangeAllChildrenInScrollView();
            }

            @Override
            public void onFailure(Call<ElementEntity[]> call, Throwable t) {
                Toast.makeText(ManagerActivity.this, "Failure to find children", Toast.LENGTH_SHORT).show();
                Log.i("TAG", "onFailure: " + t);
                return;
            }
        });
    }

    // setup the table
    private void arrangeAllChildrenInScrollView() {
        createTableLayout();
        startLoadData();
    }

    private void createTableLayout() {
        mTableLayout = (TableLayout) findViewById(R.id.tableInvoices);
        mTableLayout.setStretchAllColumns(true);
    }

    private TableLayout.LayoutParams createTableLayoutParams() {
        TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParams.setMargins(0, 0, 0, 0);
        return trParams;
    }

    public void startLoadData() {
        int numOfRows = children.length;
        TextView textSpacer = null;
        //Clean the table before adding all rows UI.
        mTableLayout.removeAllViews();

        // -1 means heading row
        for (int i = -1; i < numOfRows; i++) {
            ElementEntity elementEntity = null;
            if (i > -1) {
                elementEntity = children[i];
                elementId.put(i,elementEntity.getElementId());
            } else {
                textSpacer = new TextView(this);
                textSpacer.setText("");
            }

            //create data columns
            TextView elementName = createCellUI(i, "Name", elementEntity);
            TextView elementActive = createCellUI(i, "Active", elementEntity);
            TextView elementTimeStamp = createCellUI(i, "Created Time", elementEntity);
            TextView elementLocation = createCellUI(i, "Location", elementEntity);

            //Create tableLayout params
            TableLayout.LayoutParams trParams = createTableLayoutParams();

            //Create row UI
            TableRow tr = createRowUI(elementName, elementActive, elementTimeStamp, elementLocation, i, trParams);
            mTableLayout.addView(tr, trParams);
            if (i > -1) // if it is not the header
                listenersOfTextViews(i,mTableLayout, tr, elementName, elementActive, elementTimeStamp, elementLocation);
        }
    }

    private void listenersOfTextViews(int indexOfElement,TableLayout mTableLayout, TableRow tr, TextView elementName, TextView elementActive, TextView elementTimeStamp, TextView elementLocation) {
        tr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!updateParkingBtn.isEnabled()) {
                    updateParkingBtn.setEnabled(true);
                    updateParkingBtn.setTextColor(Color.parseColor("#FBB03B"));
                    tr.setBackgroundColor(Color.parseColor("#FBB03B"));
                    mTableLayout.setEnabled(false);
                    mTableLayout.setClickable(false);
                    editChoice.setVisibility(View.VISIBLE);

                    // GET Specific ELEMENT
                    Call<ElementEntity> specElement = elementService.getElement(
                            managerElementEntity.getElementId().getDomain(),
                            managerUserEntity.getUserId().getEmail(),
                            elementId.get(indexOfElement).getDomain(),
                            elementId.get(indexOfElement).getId());

                    specElement.enqueue(new Callback<ElementEntity>() {
                        @Override
                        public void onResponse(Call<ElementEntity> call, Response<ElementEntity> response) {
                            Log.i("TAG", "onResponse: " + response.code());
                            Toast.makeText(ManagerActivity.this, "Success on fetching specific element", Toast.LENGTH_SHORT).show();
                            // GET ARRAY of all children of Manager
                            Log.d("simba", response.body().toString());
                            parkToUpdate = response.body();
//                            parkToUpdate.setName(elementName.getText().toString().trim());
//                            parkToUpdate.setActive(elementActive.getText().toString().trim() == "yes" ?true:false);
//                            String st = elementLocation.getText().toString().trim().replace("(","");
//                            st = st.replace(")","");
//                            String[] latlng = st.split(",");
//                            parkToUpdate.setLocation(new Location(
//                                    Double.parseDouble(latlng[0]),
//                                    Double.parseDouble(latlng[1])));
                        }

                        @Override
                        public void onFailure(Call<ElementEntity> call, Throwable t) {
                            Toast.makeText(ManagerActivity.this, "Failure to fetching specific element", Toast.LENGTH_SHORT).show();
                            Log.i("TAG", "onFailure: " + t);
                            return;
                        }
                    });
                }
            }
        });
    }

    private TableRow createRowUI(TextView elementName,
                                 TextView elementActive,
                                 TextView elementTimeStamp,
                                 TextView elementLocation,
                                 int index,
                                 TableLayout.LayoutParams trParams) {

        final TableRow tr = new TableRow(this);
        tr.setId(index + 1);
        tr.setPadding(0, 0, 0, 0);
        tr.setLayoutParams(trParams);
        tr.addView(elementName);
        tr.addView(elementActive);
        tr.addView(elementTimeStamp);
        tr.addView(elementLocation);

        return tr;
    }

    private TextView createCellUI(int index, String headerName, ElementEntity rowData) {
        int textSize = (int) getResources().getDimension(R.dimen.font_size_small);
        int smallTextSize = (int) getResources().getDimension(R.dimen.font_size_small);
        final TextView tv = new TextView(this);
        tv.setLayoutParams(new TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tv.setGravity(Gravity.CENTER);
        tv.setPadding(0, 0, 0, 0);

        if (index == -1) { // Header Column.
            tv.setText(headerName);
            tv.setBackgroundColor(Color.parseColor("#29ABE2"));
            tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
        } else { // Data Column
            if (headerName.equals("Name")) {
                tv.setText(rowData.getName());
            } else {
                if (headerName.equals("Active"))
                    tv.setText(rowData.getActive().toString());
                else {
                    if (headerName.equals("Created Time"))
                        tv.setText(rowData.getCreatedTimestamp().toString());
                    else
                        tv.setText("(" + rowData.getLocation().getLat() + ", " + rowData.getLocation().getLng() + ")");
                }
            }
            tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        }
        tv.setTextColor(Color.BLACK);
        return tv;
    }
}