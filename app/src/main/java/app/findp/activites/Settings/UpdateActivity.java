package app.findp.activites.Settings;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.HashMap;

import app.findp.R;
import app.findp.entities.ElementEntity;
import app.findp.entities.UserEntity;
import app.findp.entities.utils.ElementId;
import app.findp.entities.utils.Location;
import app.findp.entities.utils.Util;
import app.findp.service.ElementService;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class UpdateActivity extends AppCompatActivity {

    public final static String PARK_ELEMENT_TO_UPDATE = "park element to update";
    private ElementEntity parkToUpdate;
    public final static String ELEMENT = "element";
    public final static String USER = "user";
    private ElementService elementService;
    private TextInputEditText nameTextBX;
    private TextInputEditText latTextBX;
    private TextInputEditText lngTextBX;
    private RadioButton active;
    private RadioButton notActive;
    private Button updateParkingBtn;
    private ElementEntity managerElementEntity;
    private UserEntity userElementEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        nameTextBX = (TextInputEditText)findViewById(R.id.update_name);
        latTextBX = (TextInputEditText)findViewById(R.id.update_lat);
        lngTextBX = (TextInputEditText)findViewById(R.id.update_lng);
        active = (RadioButton)findViewById(R.id.update_active);
        notActive = (RadioButton) findViewById(R.id.update_not_active);
        updateParkingBtn = (Button)findViewById(R.id.update_parking_btn);
        managerElementEntity = (ElementEntity) getIntent().getSerializableExtra(ELEMENT);
        userElementEntity = (UserEntity) getIntent().getSerializableExtra(USER);
        // static method
        OkHttpClient okHttpClient = Util.createOkHttpClient();
        Retrofit retrofit = Util.createRetrofit(okHttpClient, ELEMENT);
        elementService = retrofit.create(ElementService.class);
        // Get ELEMENT parking
        parkToUpdate = (ElementEntity) getIntent().getSerializableExtra(PARK_ELEMENT_TO_UPDATE);

        initData();
    }

    private void initData() {
        // put all details in the correct fields
        nameTextBX.setText(parkToUpdate.getName());
        latTextBX.setText(parkToUpdate.getLocation().getLat().toString());
        lngTextBX.setText(parkToUpdate.getLocation().getLng().toString());
        if (parkToUpdate.getActive() == true) {
            active.setChecked(true);
            notActive.setChecked(false);
        }
        else {
            notActive.setChecked(true);
            active.setChecked(false);
        }

        updateParkingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateParking();

//                ElementEntity newElementAfterChange = new ElementEntity(parkToUpdate.getElementId(),
//                        parkToUpdate.getType(),
//                        nameTextBX.getText().toString(),
//                        active.isChecked() == true ? true:false,
//                        parkToUpdate.getCreatedTimestamp(),
//                        parkToUpdate.getCreatedBy(),
//                        new Location( Double.parseDouble(latTextBX.getText().toString()),
//                                Double.parseDouble(lngTextBX.getText().toString())),
//                        new HashMap<>());

                parkToUpdate.setName(nameTextBX.getText().toString().trim());
                parkToUpdate.setActive(active.isChecked() == true ? true:false);
//                String st = elementLocation.getText().toString().trim().replace("(","");
//                st = st.replace(")","");
//                String[] latlng = st.split(",");
                parkToUpdate.setLocation( new Location( Double.parseDouble(latTextBX.getText().toString()), Double.parseDouble(lngTextBX.getText().toString())));
                // update parking place in City(Manager)
                Call<Void> updateExistingPark = elementService.updateElement(
                        managerElementEntity.getElementId().getDomain(),
                        userElementEntity.getUserId().getEmail(),
                        parkToUpdate.getElementId().getDomain(),
                        parkToUpdate.getElementId().getId(),
                        parkToUpdate);

                updateExistingPark.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Log.i("TAG", "onResponse: " + response.code());
                        Toast.makeText(UpdateActivity.this, "** Success on UPDATE **", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(UpdateActivity.this, "** Failure to UPDATE **", Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onFailure: " + t);
                        return;
                    }
                });
            }
        });
    }

    private void updateParking() {
        parkToUpdate.setName(nameTextBX.getText().toString().trim());
        parkToUpdate.setLocation(new Location(
                Double.parseDouble(latTextBX.getText().toString()),
                Double.parseDouble(lngTextBX.getText().toString())));
        parkToUpdate.setActive(active.equals("Yes") ? true : false);
    }
}