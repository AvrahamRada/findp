package app.findp.activites.Map;

import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import app.findp.R;
import app.findp.activites.Parking.ResultsActivity;
import app.findp.activites.Parking.ScanningActivity;
import app.findp.data.ParkingData;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    GoogleMap mapAPI;
    SupportMapFragment mapFragment;
    ImageView back_btn;

    ParkingData[] data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        back_btn = (ImageView) findViewById(R.id.back_image_2);

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_API);
        mapFragment.getMapAsync(this);

        data = (ParkingData[]) getIntent().getSerializableExtra("PARKING_DATA_CLASS");

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backToResultActivityIntent = new Intent(MapActivity.this, ResultsActivity.class);
                startActivity(backToResultActivityIntent);
                finish();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapAPI = googleMap;

        for (int i = 0; i < data.length; i++) {
            LatLng location = new LatLng(data[i].getLocation().getLat(), data[i].getLocation().getLng());
            mapAPI.addMarker(new MarkerOptions().position(location).title("address" + (i+1)));
        }
        mapAPI.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(ScanningActivity.userLocationMockup.getLat(), ScanningActivity.userLocationMockup.getLng()), 16));
    }
}
