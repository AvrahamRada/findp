package app.findp.activites.Start;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.findp.R;
import app.findp.activites.Parking.MainActivity;
import app.findp.activites.Settings.ManagerActivity;
import app.findp.entities.ActionEntity;
import app.findp.entities.ElementEntity;
import app.findp.entities.UserEntity;
import app.findp.entities.utils.Element;
import app.findp.entities.utils.ElementId;
import app.findp.entities.utils.InvokedBy;
import app.findp.entities.utils.UserId;
import app.findp.entities.utils.Util;
import app.findp.entities.utils.CreatedBy;
import app.findp.entities.utils.Location;
import app.findp.entities.utils.NewUserDetails;
import app.findp.entities.utils.UserRole;
import app.findp.entities.utils.Validator;
import app.findp.service.ActionService;
import app.findp.service.ElementService;
import app.findp.service.UserService;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RegistrationActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private TextInputLayout editText_email;
    private TextInputLayout editText_userName;
    private ImageView avatar_imageView;
    private Spinner role_spinner;
    private Button create_btn;

    private Uri url;
    private String userType;
    private UserService userService;
    private ElementService elementService;
    private ActionService actionService;
    // listView
    private ListView listView;
    private String avatar;
    private int[] image = {R.drawable.avatar01,R.drawable.avatar02,R.drawable.avatar03,R.drawable.avatar04,R.drawable.avatar05,R.drawable.avatar06};
    private String[] headline = {"Student","Worker","Anonymous","City employee","Professor","Teacher"};
    private String[] drawableName = {"avatar01","avatar02","avatar03","avatar04","avatar05","avatar06"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        init();
        listeners();
    }

    private void init() {
        editText_email = (TextInputLayout) findViewById(R.id.editText_email);
        editText_userName = (TextInputLayout) findViewById(R.id.editText_userName);
        avatar_imageView = (ImageView) findViewById(R.id.avatar_imageView);
        create_btn = (Button) findViewById(R.id.create_btn);
        role_spinner = (Spinner) findViewById(R.id.roleSpinner);

        //spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.role_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        role_spinner.setAdapter(adapter);
        role_spinner.setOnItemSelectedListener(this);

        // static method
        OkHttpClient okHttpClient = Util.createOkHttpClient();
        Retrofit retrofit = Util.createRetrofit(okHttpClient, "user");
        userService = retrofit.create(UserService.class);
        Retrofit retrofit2 = Util.createRetrofit(okHttpClient, "element");
        elementService = retrofit2.create(ElementService.class);
        Retrofit retrofit3 = Util.createRetrofit(okHttpClient, "action");
        actionService = retrofit3.create(ActionService.class);

        // ListView
        listView = findViewById(R.id.list_view);
        List<HashMap<String, String>> list = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("image", Integer.toString(image[i]));
            hashMap.put("headline", headline[i]);
            list.add(hashMap);
        }

        String[] from = {"image", "headline"};
        int[] to = {R.id.imageViewLogo, R.id.textViewHeadline1};

        SimpleAdapter simpleAdapter = new SimpleAdapter(getBaseContext(), list, R.layout.list_view_activity, from, to);
        listView.setAdapter(simpleAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                avatar = drawableName[position];
            }
        });

    }

    private void listeners() {
        create_btn.setOnClickListener(view -> {
            if(!Validator.isValidEmail(editText_email.getEditText().getText().toString().trim())){
                editText_email.setError("Invalid Email");
                return;
            }
            if(!Validator.isValidUserName(editText_userName.getEditText().getText().toString().trim())){
                editText_userName.setError("Invalid User name");
                return;
            }
            if(avatar == null || !Validator.isValidAvatarUrl(avatar)){
                Toast.makeText(RegistrationActivity.this,"Choose avatar", Toast.LENGTH_SHORT).show();
                return;
            }

            NewUserDetails newUser = new NewUserDetails(editText_email.getEditText().getText().toString().trim()
                    , UserRole.valueOf(userType),editText_userName.getEditText().getText().toString().trim(),avatar);
            createUser(newUser);
        });
    }

    private void createUser(NewUserDetails newUser) {
        if (!newUser.getRole().equals(UserRole.MANAGER)) {
            Call<UserEntity> callUser = userService.createNewUser(newUser);
            callUser.enqueue(new Callback<UserEntity>() {
                @Override
                public void onResponse(Call<UserEntity> call, Response<UserEntity> response) {
                    if (!response.isSuccessful()) {
                        Log.i("TAG", "onResponse: " + response.code());
                        Toast.makeText(RegistrationActivity.this, "Something wrong: " + response.toString(), Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        Log.i("TAG", "onResponse: " + response.code());
                        Toast.makeText(RegistrationActivity.this, "Create Player User", Toast.LENGTH_SHORT).show();
                        UserEntity userEntity = response.body();
                        Intent mainActivityIntent = new Intent(RegistrationActivity.this, MainActivity.class);
                        mainActivityIntent.putExtra("user", userEntity);
                        startActivity(mainActivityIntent);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<UserEntity> call, Throwable t) {
                    Toast.makeText(RegistrationActivity.this, "Failure create User", Toast.LENGTH_SHORT).show();
                    Log.i("TAG", "onFailure: " + t);
                    return;
                }
            });
        } else {
            ActionEntity actionEntity = createActionForManager(newUser);
            Call<Object> callAction = actionService.invokeAction(actionEntity);
            callAction.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    if(!response.isSuccessful()){
                        if(response.code() == 500){
                            Log.i("TAG", "onResponse: " + response.code());
                            Toast.makeText(RegistrationActivity.this, "Manger already exist", Toast.LENGTH_SHORT).show();
                            return;
                        }else {
                            Log.i("TAG", "onResponse: " + response.code());
                            Toast.makeText(RegistrationActivity.this, "Something wrong: " + response.toString(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }else{
                        UserEntity manager = convertObjectToEntity(response.body());
                        Call<ElementEntity[]> callElement = elementService.getAllElementsByName(manager.getUserId().getDomain(),manager.getUserId().getEmail(),
                                manager.getUsername(),1,0);
                        callElement.enqueue(new Callback<ElementEntity[]>() {
                            @Override
                            public void onResponse(Call<ElementEntity[]> call, Response<ElementEntity[]> response) {
                                if (!response.isSuccessful()) {
                                    Log.i("TAG", "onResponse: " + response.code());
                                    Toast.makeText(RegistrationActivity.this, "Something wrong: " + response.toString(), Toast.LENGTH_SHORT).show();
                                    return;
                                }else{
                                    ElementEntity[] managerElement = response.body();
                                    Toast.makeText(RegistrationActivity.this, "Success Element user", Toast.LENGTH_SHORT).show();
                                    Log.i("TAG", "onResponse: " + response.code());
                                    Intent managerActivityIntent = new Intent(RegistrationActivity.this, ManagerActivity.class);
                                    managerActivityIntent.putExtra("user", manager);
                                    managerActivityIntent.putExtra("element", managerElement[0]);
                                    startActivity(managerActivityIntent);
                                    finish();
                                }
                            }

                            @Override
                            public void onFailure(Call<ElementEntity[]> call, Throwable t) {
                                Toast.makeText(RegistrationActivity.this, "Failure Manager User", Toast.LENGTH_SHORT).show();
                                Log.i("TAG", "onFailure: " + t);
                                return;
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Toast.makeText(RegistrationActivity.this, "Failure invoke action", Toast.LENGTH_SHORT).show();
                    Log.i("TAG", "onFailure: " + t);
                    return;
                }
            });
        }
    }

    private UserEntity convertObjectToEntity(Object response) {
        ObjectMapper mapper = new ObjectMapper();
        UserEntity manager = mapper.convertValue(response, new TypeReference<UserEntity>(){});
        return manager;
    }

    private ActionEntity createActionForManager(NewUserDetails newUser){
        Map <String,Object> hm = new HashMap<>();
        hm.put("email",newUser.getEmail());
        hm.put("role",newUser.getRole());
        hm.put("username",newUser.getUsername());
        hm.put("avatar",newUser.getAvatar());
        return new ActionEntity(null,"createUserManagerByUsername",new Element(new ElementId("","")),new Date(),
                new InvokedBy(new UserId(Util.domain,newUser.getEmail())),hm);
    }

    private ElementEntity createElementForManger(UserEntity userEntity) {
        Map <String,Object> hm = new HashMap<>();
        return new ElementEntity(null,"demo", userEntity.getUsername(), true, new Date(),
                new CreatedBy(userEntity.getUserId()), new Location(999999.0, 999999.0),hm);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        userType = parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(),userType,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case Util.GALLERY_REQUEST:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    avatar_imageView.setImageURI(data.getData());
                    url = data.getData();
                }
        }
    }
}