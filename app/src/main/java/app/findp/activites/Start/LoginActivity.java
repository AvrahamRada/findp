package app.findp.activites.Start;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import java.util.HashMap;

import app.findp.R;
import app.findp.activites.Parking.MainActivity;
import app.findp.activites.Parking.UnParkingActivity;
import app.findp.activites.Settings.ManagerActivity;
import app.findp.entities.ActionEntity;
import app.findp.entities.ElementEntity;
import app.findp.entities.UserEntity;
import app.findp.entities.utils.Element;
import app.findp.entities.utils.ElementId;
import app.findp.entities.utils.InvokedBy;
import app.findp.entities.utils.UserRole;
import app.findp.entities.utils.Util;
import app.findp.entities.utils.Validator;
import app.findp.service.ActionService;
import app.findp.service.ElementService;
import app.findp.service.UserService;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class
LoginActivity extends AppCompatActivity {
    public final static String USER = "user";
    public final static String ELEMENT = "element";
    public final static String ACTION = "action";
    private final String IS_USER_PARKED = "isUserParked";
    private TextInputLayout editText_email;
    private Button login_btn;
    private TextView signUp_textView;

    private UserService userService;
    private ElementService elementService;
    private ActionService actionService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        listeners();
    }

    private void init() {
        editText_email = (TextInputLayout) findViewById(R.id.editText_email);
        login_btn = (Button) findViewById(R.id.login_btn);
        signUp_textView = (TextView) findViewById(R.id.signUp_textView);

        // static method
        OkHttpClient okHttpClient = Util.createOkHttpClient();
        Retrofit retrofit = Util.createRetrofit(okHttpClient,USER);
        userService = retrofit.create(UserService.class);
        Retrofit retrofit2 = Util.createRetrofit(okHttpClient,ELEMENT);
        elementService = retrofit2.create(ElementService.class);
        Retrofit retrofit3 = Util.createRetrofit(okHttpClient,ACTION);
        actionService = retrofit3.create(ActionService.class);

    }

    private void listeners() {
        login_btn.setOnClickListener(view -> {
            if (!Validator.isValidEmail(editText_email.getEditText().getText().toString().trim())) {
                editText_email.setError("Invalid Email");
                return;
            }
            Call<UserEntity> callUser = userService.login(Util.domain,editText_email.getEditText().getText().toString().trim());
            callUser.enqueue(new Callback<UserEntity>() {
                @Override
                public void onResponse(Call<UserEntity> call, Response<UserEntity> response) {
                    if (!response.isSuccessful()) {
                        Log.i("TAG", "onResponse: " + response.code());
                        Toast.makeText(LoginActivity.this, "Something wrong: " + response.toString(), Toast.LENGTH_SHORT).show();
                        return;
                    }else{
                        Log.i("TAG", "onResponse: " + response.code());
                        Toast.makeText(LoginActivity.this, "Login User", Toast.LENGTH_SHORT).show();
                        UserEntity userEntity = response.body();
                        if (!userEntity.getRole().equals(UserRole.MANAGER)){
                            isUserParked(userEntity);
                        }else {
                            Call<ElementEntity[]> checkManager = elementService.getAllElementsByName(userEntity.getUserId().getDomain(),
                                    userEntity.getUserId().getEmail(), userEntity.getUsername(), 1, 0);
                            checkManager.enqueue(new Callback<ElementEntity[]>() {
                                @Override
                                public void onResponse(Call<ElementEntity[]> call, Response<ElementEntity[]> response) {
                                    Log.i("TAG", "onResponse: " + response.code());
                                    Toast.makeText(LoginActivity.this, "Find element", Toast.LENGTH_SHORT).show();
                                    ElementEntity[] managerElementEntity = response.body();
                                    Intent managerActivityIntent = new Intent(LoginActivity.this, ManagerActivity.class);
                                    managerActivityIntent.putExtra(USER, userEntity);
                                    managerActivityIntent.putExtra(ELEMENT, managerElementEntity[0]);
                                    startActivity(managerActivityIntent);
                                    finish();
                                }

                                @Override
                                public void onFailure(Call<ElementEntity[]> call, Throwable t) {
                                    Toast.makeText(LoginActivity.this, "Failure create User", Toast.LENGTH_SHORT).show();
                                    Log.i("TAG", "onFailure: " + t);
                                    return;
                                }
                            });
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserEntity> call, Throwable t) {
                    Toast.makeText(LoginActivity.this, "Failure create User", Toast.LENGTH_SHORT).show();
                    Log.i("TAG", "onFailure: " + t);
                    return;
                }
            });

        });
        signUp_textView.setOnClickListener(view -> {
            Intent registrationActivityIntent = new Intent(LoginActivity.this, RegistrationActivity.class);
            startActivity(registrationActivityIntent);
        });
    }

    private void isUserParked(UserEntity userEntity){
        Call<Object> actionCall = actionService.invokeAction(
                new ActionEntity(null, IS_USER_PARKED, new Element(new ElementId("","")),
                        null, new InvokedBy(userEntity.getUserId()), new HashMap<>()));
        actionCall.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {
                    boolean isUserParked = (Boolean) response.body();
                    Log.i("TAG", "onResponse: " + response.code() + "  " + "  " + response.errorBody() + "  " + response.message().getClass() + "  " + response.body().getClass());
                    Toast.makeText(LoginActivity.this, "Something ok: " + response.message(), Toast.LENGTH_SHORT).show();
                    Log.d("TEST", "onResponse: " + isUserParked);
                    if(isUserParked){
                        Intent unparkingActivityIntent = new Intent(LoginActivity.this, UnParkingActivity.class);
                        unparkingActivityIntent.putExtra(LoginActivity.USER, userEntity);
                        startActivity(unparkingActivityIntent);
                        finish();
                    }else {
                        Intent parkingActivityIntent = new Intent(LoginActivity.this, MainActivity.class);
                        parkingActivityIntent.putExtra(LoginActivity.USER, userEntity);
                        startActivity(parkingActivityIntent);
                        finish();
                    }
                } else {
                    Log.i("TAG", "onResponse: " + response.code() + "  " + "  " + response.errorBody() + "  " + response.message() + "  " + response.body());
                    Intent parkingActivityIntent = new Intent(LoginActivity.this, MainActivity.class);
                    parkingActivityIntent.putExtra(LoginActivity.USER, userEntity);
                    startActivity(parkingActivityIntent);
                    finish();
                }
            }
            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Failure getAll", Toast.LENGTH_SHORT).show();
                Log.i("TAG", "onFailure: " + t);
                return;
            }
        });
    }

}

