package app.findp.activites.Parking;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import app.findp.R;
import app.findp.activites.Settings.SettingsActivity;
import app.findp.activites.Start.LoginActivity;
import app.findp.entities.UserEntity;

public class MainActivity extends AppCompatActivity {

    private ImageView avatar_imageView;
    private ImageView logout_btn;
    private ImageView search_btn;
    private TextView user_role;
    private Button settings_btn;
    private UserEntity playerUserEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        listeners();
    }

    private void init() {
        avatar_imageView = (ImageView) findViewById(R.id.avatar_imageView);
        logout_btn = (ImageView) findViewById(R.id.logout_btn);
        search_btn = (ImageView) findViewById(R.id.search_button);
        user_role = (TextView) findViewById(R.id.user_name);
        settings_btn = (Button) findViewById(R.id.settings_button);

        playerUserEntity = (UserEntity) getIntent().getSerializableExtra(LoginActivity.USER);
        user_role.setText(playerUserEntity.getUsername());

        Context context = avatar_imageView.getContext();
        int id = context.getResources().getIdentifier(playerUserEntity.getAvatar(), "drawable", MainActivity.this.getPackageName());
        avatar_imageView.setImageResource(id);
    }

    private void listeners() {
        logout_btn.setOnClickListener(v -> {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        });
        search_btn.setOnClickListener(v -> {
            Intent scanningActivityIntent = new Intent(MainActivity.this, ScanningActivity.class);
            scanningActivityIntent.putExtra(LoginActivity.USER, playerUserEntity);
            startActivity(scanningActivityIntent);
        });
        settings_btn.setOnClickListener(v -> {
            Intent settingActivityIntent = new Intent(MainActivity.this, SettingsActivity.class);
            settingActivityIntent.putExtra(LoginActivity.USER, playerUserEntity);
            startActivity(settingActivityIntent);
        });

    }
}
