package app.findp.activites.Parking;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import app.findp.R;
import app.findp.activites.Start.LoginActivity;
import app.findp.data.ParkingData;
import app.findp.entities.ElementEntity;
import app.findp.entities.UserEntity;
import app.findp.entities.utils.Location;
import app.findp.entities.utils.Util;
import app.findp.rest.RestAPI;
import app.findp.service.ElementService;
import app.findp.service.UserService;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ScanningActivity extends AppCompatActivity {
    public static final String ELEMENT_ENTITIES = "elementEntities";
    public final String IS_TAKEN = "isTaken";
    final String DISTANCE = "1";
    private ImageView iconImg;
    private TextView scanningMsg;
    private UserEntity playerUserEntity;
    private ElementService elementService;
    private ParkingData[] parkingData;
    private double MyLatMockup = 32.1150148;
    private double MyLngMockup =34.8162585;
    public static Location userLocationMockup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanning);
        init();
//        listeners();
        rotate();
//        getAllElementsByLocation();

    }

    private void init() {
        scanningMsg = (TextView) findViewById(R.id.scan_msg);
        iconImg = (ImageView) findViewById(R.id.icon_image);
        // static method
        OkHttpClient okHttpClient = Util.createOkHttpClient();
        Retrofit retrofit2 = Util.createRetrofit(okHttpClient, LoginActivity.ELEMENT);
        elementService = retrofit2.create(ElementService.class);
        playerUserEntity = (UserEntity) getIntent().getSerializableExtra(LoginActivity.USER);
        userLocationMockup = new Location(MyLatMockup,MyLngMockup);
    }

    RotateAnimation rotateAnimation;
    private void rotate() {
        rotateAnimation = new RotateAnimation(0, 360,
                RotateAnimation.RELATIVE_TO_SELF, .5f,
                RotateAnimation.RELATIVE_TO_SELF, .5f);

        rotateAnimation.setDuration(2000);
        rotateAnimation.setRepeatCount(Animation.ABSOLUTE);
        rotateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                scanningMsg.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                getAllElementsByLocation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                if (scanningMsg.getVisibility() == View.INVISIBLE)
                    scanningMsg.setVisibility(View.VISIBLE);
                else
                    scanningMsg.setVisibility(View.INVISIBLE);
            }
        });
        iconImg.startAnimation(rotateAnimation);
    }

    public void getAllElementsByLocation() {
        Call<ElementEntity[]> elementsCall = elementService.getAllElementsByLocation(playerUserEntity.getUserId().getDomain(),
                playerUserEntity.getUserId().getEmail(), "32.1149197", "34.8159152", DISTANCE, 10, 0);
        elementsCall.enqueue(new Callback<ElementEntity[]>() {
            @Override
            public void onResponse(Call<ElementEntity[]> call, Response<ElementEntity[]> response) {
                if (response.isSuccessful()) {
                    Log.i("TAG", "onResponse: " + response.code() + "  " + "  " + response.errorBody() + "  " + response.message().getClass() + "  " + response.body().getClass());
                    Toast.makeText(ScanningActivity.this, "Something ok: " + response.message(), Toast.LENGTH_SHORT).show();
                    ElementEntity[] elementsEntities = filterTakenPark(response.body());
                    if(elementsEntities.length > 0) {
                        parkingData = convertElementsToParking(elementsEntities);
                        sortByDistance(parkingData);
                        Intent scanningActivityIntent = new Intent(ScanningActivity.this, ResultsActivity.class);
                        scanningActivityIntent.putExtra(ELEMENT_ENTITIES, parkingData);
                        scanningActivityIntent.putExtra(LoginActivity.USER, playerUserEntity);
                        startActivity(scanningActivityIntent);
//                        iconImg.startAnimation(rotateAnimation);
                        finish();

                    }else {
                        Intent scanningActivityIntent = new Intent(ScanningActivity.this, SorryActivity.class);
                        scanningActivityIntent.putExtra(LoginActivity.USER, playerUserEntity);
                        startActivity(scanningActivityIntent);
                        finish();
                    }

                }else{
                    Log.i("TAG", "onResponse: " + response.code() + "  " + "  " + response.errorBody() + "  " + response.message() + "  " + response.body());
                    Toast.makeText(ScanningActivity.this, "Something not ok: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ElementEntity[]> call, Throwable t) {
                Toast.makeText(ScanningActivity.this, "Failure getAll", Toast.LENGTH_SHORT).show();
                Log.i("TAG", "onFailure: " + t);
                return;
            }
        });
    }

    private ElementEntity[] filterTakenPark(ElementEntity[] elementEntityData){
        List<ElementEntity> list = new ArrayList<>();
        for(ElementEntity elementEntity :elementEntityData){
            if(!((Boolean)elementEntity.getElementAttributes().get(IS_TAKEN))){
                list.add(elementEntity);
            }
        }
        return list.toArray(new ElementEntity[0]);
    }

    private ParkingData[] convertElementsToParking(ElementEntity[] elementEntityData) {
        ParkingData[] parkingData = new ParkingData[elementEntityData.length];
        for (int i = 0; i < elementEntityData.length; i++) {
            parkingData[i] = new ParkingData(elementEntityData[i].getName(), elementEntityData[i].getLocation(),
                    elementEntityData[i].getElementId(),Boolean.parseBoolean(elementEntityData[i].getElementAttributes().get(IS_TAKEN).toString()));
            parkingData[i].calculateDistance(userLocationMockup);
        }
        return parkingData;
    }

    public void sortByDistance(ParkingData[] arr){
        int n = arr.length;
        for (int i = 0; i < n-1; i++)
            for (int j = 0; j < n-i-1; j++)
                if (arr[j].getDistance() > arr[j+1].getDistance())
                {
                    // swap arr[j+1] and arr[i]
                    double temp = arr[j].getDistance();
                    arr[j].setDistance(arr[j+1].getDistance());
                    arr[j+1].setDistance(temp);
                }
    }
}
