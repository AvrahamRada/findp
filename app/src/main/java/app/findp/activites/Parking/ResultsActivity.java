package app.findp.activites.Parking;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import app.findp.activites.Map.MapActivity;
import app.findp.R;
import app.findp.activites.Start.LoginActivity;
import app.findp.data.ParkingData;
import app.findp.entities.ElementEntity;
import app.findp.entities.UserEntity;

public class ResultsActivity extends AppCompatActivity {

    private TableLayout mTableLayout;
    public static String PARKING_DATA = "parkingData";
    //ProgressDialog mProgressBar;
//    private ElementEntity[] data;
    private ParkingData[] data;
    private UserEntity playerUserEntity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        //mProgressBar = new ProgressDialog(this);

        data = (ParkingData[]) getIntent().getSerializableExtra(ScanningActivity.ELEMENT_ENTITIES);
        playerUserEntity = (UserEntity) getIntent().getSerializableExtra(LoginActivity.USER);
        // setup the table
        createTableLayout();
        startLoadData();
    }

    private void createTableLayout() {
        mTableLayout = (TableLayout) findViewById(R.id.tableInvoices);
        mTableLayout.setStretchAllColumns(true);
    }

    private TableLayout.LayoutParams createTableLayoutParams() {

        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;

        TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
        return trParams;
    }

    public void startLoadData() {
        loadData();
    }

    public void loadData() {

        int numOfRows = data.length;
        TextView textSpacer = null;

        //Clean the table before adding all rows UI.
        mTableLayout.removeAllViews();

        // -1 means heading row
        for (int i = -1; i < numOfRows; i++) {

            ParkingData parkingData = null;

            if (i > -1) {
                parkingData = data[i];
            } else {
                textSpacer = new TextView(this);
                textSpacer.setText("");
            }

            //create data columns
            TextView placesTV = createCellUI(i, "Place", parkingData);
            TextView distanceTV = createCellUI(i, "Distance", parkingData);


            //Create tableLayout params
            TableLayout.LayoutParams trParams = createTableLayoutParams();

            //Create row UI
            TableRow tr = createRowUI(placesTV, distanceTV, i, trParams);

            if (i > -1) {
                tr.setTag(i);
                tr.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        TableRow tr = (TableRow)v;

                        Intent resultsActivityIntent = new Intent(ResultsActivity.this, ParkingActivity.class);
                        resultsActivityIntent.putExtra(PARKING_DATA,data[(int)tr.getTag()]);
                        resultsActivityIntent.putExtra(LoginActivity.USER, playerUserEntity);
                        startActivity(resultsActivityIntent);

                    }
                });
            }

            mTableLayout.addView(tr, trParams);
        }
    }

    private void ChooseParkingPlace() {
        //TODO- Moving to success parking place
    }

    private TableRow createRowUI(TextView placesTV, TextView distanceTV, int index, TableLayout.LayoutParams trParams) {

        final TableRow tr = new TableRow(this);
        tr.setId(index + 1);

        tr.setPadding(0, 0, 0, 0);
        tr.setLayoutParams(trParams);

        tr.addView(placesTV);
        tr.addView(distanceTV);
        //tr.addView(layAmounts);

        return tr;
    }

    private TextView createCellUI(int index, String headerName, ParkingData rowData) {

        int textSize = (int) getResources().getDimension(R.dimen.font_size_verysmall);
        int smallTextSize = (int) getResources().getDimension(R.dimen.font_size_small);

        final TextView tv = new TextView(this);

        tv.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT));

        tv.setGravity(Gravity.CENTER);
        tv.setPadding(5, 15, 0, 15);

        if (index == -1) {
            //Header Column.
            tv.setText(headerName);
            tv.setBackgroundColor(Color.parseColor("#f0f0f0"));
            tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
        } else {
            //Data Column
            tv.setBackgroundColor(Color.parseColor("#f8f8f8"));
            if (headerName.equals("Place")) {
                tv.setText(rowData.getName());
            } else {
                tv.setText("" + rowData.getDistance() + " KM");
            }
            tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        }

        return tv;
    }

    public void goToMap(View view) {
        Intent mapActivityIntent = new Intent(ResultsActivity.this, MapActivity.class);
        mapActivityIntent.putExtra("PARKING_DATA_CLASS", data);
        startActivity(mapActivityIntent);

    }
}