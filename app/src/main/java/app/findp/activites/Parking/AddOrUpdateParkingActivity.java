package app.findp.activites.Parking;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.HashMap;
import java.util.Map;

import app.findp.R;
import app.findp.activites.Settings.ManagerActivity;
import app.findp.entities.ElementEntity;
import app.findp.entities.UserEntity;
import app.findp.entities.utils.Location;
import app.findp.entities.utils.Util;
import app.findp.general.ViewDialog;
import app.findp.service.ElementService;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AddOrUpdateParkingActivity extends AppCompatActivity {

    private static final String ADD_PARKING = "ADD PARKING";
    private static final String DEFAULT_TYPE = "park";
    public final static String ELEMENT = "element";
    public final static String USER = "user";
    private static final String DISTANCE = "0";

    private ElementEntity elementEntity;
    private Button addBtn;
    private TextInputLayout nameTextBX;
    private TextInputLayout latTextBX;
    private TextInputLayout lngTextBX;
    private RadioGroup activeBtn;
    private ElementService elementService;

    private UserEntity managerUserEntity;
    private ElementEntity managerElementEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_update_parking);
        elementEntity = new ElementEntity();
        init();
    }

    private void init() {
        // static method
        OkHttpClient okHttpClient = Util.createOkHttpClient();
        Retrofit retrofit = Util.createRetrofit(okHttpClient,ELEMENT);
        elementService = retrofit.create(ElementService.class);

        // Get USER MANAGER
        managerUserEntity = (UserEntity) getIntent().getSerializableExtra(USER);
        // Get ELEMENT MANAGER
        managerElementEntity = (ElementEntity) getIntent().getSerializableExtra(ELEMENT);

        addBtn = (Button)findViewById(R.id.add_parking);
        nameTextBX = (TextInputLayout)findViewById(R.id.parking_name);
        latTextBX = (TextInputLayout)findViewById(R.id.parking_lat);
        lngTextBX = (TextInputLayout)findViewById(R.id.parking_lng);
        activeBtn = (RadioGroup) findViewById(R.id.radioGroup);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                elementEntity.setType(DEFAULT_TYPE);//2131362021
                String radioText = (String) ((RadioButton)findViewById(activeBtn.getCheckedRadioButtonId())).getText();

                elementEntity.setActive(radioText.equals("Yes") ? true:false);
                elementEntity.setName(nameTextBX.getEditText().getText().toString().trim());
                elementEntity.setLocation(new Location(
                        Double.parseDouble(latTextBX.getEditText().getText().toString().trim()),
                        Double.parseDouble(lngTextBX.getEditText().getText().toString().trim())));
                HashMap<String,Object> elementAttributes = new HashMap<>();
                elementAttributes.put("isTaken",false);
                elementEntity.setElementAttributes(elementAttributes);

                // Before creating parking place we need to check that in the same place their is no other parking
                checkIfParkingIsFree(
                        latTextBX.getEditText().getText().toString().trim(),
                        lngTextBX.getEditText().getText().toString().trim());
            }
        });
    }

    private void checkIfParkingIsFree(String lat,String lng) {
        // GET all children of Manager
        Call<ElementEntity[]> allParkingWithDistanceZero = elementService.getAllElementsByLocation(
                managerElementEntity.getElementId().getDomain(),
                managerUserEntity.getUserId().getEmail(),
                lat,
                lng,
                DISTANCE,
                10,
                0);

        allParkingWithDistanceZero.enqueue(new Callback<ElementEntity[]>() {
            @Override
            public void onResponse(Call<ElementEntity[]> call, Response<ElementEntity[]> response) {
                Log.i("TAG", "onResponse: " + response.code());
                Toast.makeText(AddOrUpdateParkingActivity.this, "Parking With Distance Zero", Toast.LENGTH_SHORT).show();

                ElementEntity[] distanceZero = response.body();
                if(distanceZero.length == 0) { // PARK is free
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra(ADD_PARKING, elementEntity);
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();

                }
                else { // PARK is not free
                    ViewDialog alert = new ViewDialog();
                    alert.showDialog(AddOrUpdateParkingActivity.this, "** Parking location is already in use **");
                }
            }

            @Override
            public void onFailure(Call<ElementEntity[]> call, Throwable t) {
                Toast.makeText(AddOrUpdateParkingActivity.this, "Failure to find children", Toast.LENGTH_SHORT).show();
                Log.i("TAG", "onFailure: " + t);

                return;
            }
        });
    }
}

