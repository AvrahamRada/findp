package app.findp.activites.Parking;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.drm.DrmStore;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import app.findp.R;
import app.findp.activites.Start.LoginActivity;
import app.findp.data.ParkingData;
import app.findp.entities.ActionEntity;
import app.findp.entities.ElementEntity;
import app.findp.entities.UserEntity;
import app.findp.entities.utils.ActionId;
import app.findp.entities.utils.Element;
import app.findp.entities.utils.ElementId;
import app.findp.entities.utils.InvokedBy;
import app.findp.entities.utils.Util;
import app.findp.service.ActionService;
import app.findp.service.ElementService;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ParkingActivity extends AppCompatActivity {

    private Button parkButton;
    private ParkingData parkingData;
    private ActionService actionService;
    private UserEntity userEntity;
    private final String PARK = "park";
    private final String ACTION = "action";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking);
        // static method
        OkHttpClient okHttpClient = Util.createOkHttpClient();
        Retrofit retrofit2 = Util.createRetrofit(okHttpClient, ACTION);
        actionService = retrofit2.create(ActionService.class);
        parkingData = (ParkingData) getIntent().getSerializableExtra(ResultsActivity.PARKING_DATA);
        userEntity = (UserEntity) getIntent().getSerializableExtra(LoginActivity.USER);
        Log.d("userEntity", "onCreate: " + userEntity);
        setIds();
        setClickListeners();


    }

    private void setClickListeners() {

        parkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popUpAlertDialog();
            }
        });
    }

    private void setIds() {

        parkButton = findViewById(R.id.park_btn);
    }

    private void popUpAlertDialog() {

        TextView alertMessage = new TextView(this);
        alertMessage.setGravity(Gravity.CENTER);
        alertMessage.setPadding(5, 15, 0, 15);
        alertMessage.setText("Click OK if you park");
        alertMessage.setTextColor(Color.BLACK);
        alertMessage.setTypeface(null, Typeface.BOLD);

        new AlertDialog.Builder(this)
                .setTitle("Are you sure?")
                .setView(alertMessage)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @TargetApi(11)
                            public void onClick(DialogInterface dialog, int id) {

                                parkingAction();
                                Intent parkingActivityIntent = new Intent(ParkingActivity.this, UnParkingActivity.class);
                                parkingActivityIntent.putExtra(LoginActivity.USER, userEntity);
                                startActivity(parkingActivityIntent);
                                finish();
                            }
                        })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {

                    }
                }).show();
    }


    public void parkingAction() {
        Call<Object> actionCall = actionService.invokeAction(
                new ActionEntity(null, PARK, new Element(parkingData.getElementId()),
                        null, new InvokedBy(userEntity.getUserId()), new HashMap<>()));
        actionCall.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {
                    Log.i("TAG", "onResponse: " + response.code() + "  " + "  " + response.errorBody() + "  " + response.message().getClass() + "  " + response.body().getClass());
                    Toast.makeText(ParkingActivity.this, "Something ok: " + response.message(), Toast.LENGTH_SHORT).show();
                    Intent parkingActivityIntent = new Intent(ParkingActivity.this, UnParkingActivity.class);
                    parkingActivityIntent.putExtra(LoginActivity.USER, userEntity);
                    startActivity(parkingActivityIntent);
                } else {
                    Log.i("TAG", "onResponse: " + response.code() + "  " + "  " + response.errorBody() + "  " + response.message() + "  " + response.body());
                    Toast.makeText(ParkingActivity.this, "Something not ok: " + response.message(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Toast.makeText(ParkingActivity.this, "Failure getAll", Toast.LENGTH_SHORT).show();
                Log.i("TAG", "onFailure: " + t);
                return;
            }
        });


    }

}
