package app.findp.activites.Parking;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import app.findp.R;
import app.findp.activites.Settings.ManagerActivity;
import app.findp.activites.Start.LoginActivity;
import app.findp.entities.UserEntity;

public class SorryActivity extends AppCompatActivity implements View.OnClickListener {

    private Button search_again;
    private UserEntity playerUserEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sorry);

        search_again = (Button) findViewById(R.id.search_again_btn);
        playerUserEntity = (UserEntity) getIntent().getSerializableExtra(LoginActivity.USER);
        findViewById(R.id.search_again_btn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_again_btn:
                goToMainActivity();
                break;

            default:
                break;
        }
    }

    private void goToMainActivity(){
        Intent sorryActivityIntent = new Intent(SorryActivity.this, MainActivity.class);
        sorryActivityIntent.putExtra(LoginActivity.USER, playerUserEntity);
        startActivity(sorryActivityIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goToMainActivity();
    }
}
