package app.findp.activites.Parking;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import app.findp.R;
import app.findp.activites.Start.LoginActivity;
import app.findp.data.ParkingData;
import app.findp.entities.ActionEntity;
import app.findp.entities.UserEntity;
import app.findp.entities.utils.Element;
import app.findp.entities.utils.ElementId;
import app.findp.entities.utils.InvokedBy;
import app.findp.entities.utils.Util;
import app.findp.service.ActionService;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class UnParkingActivity extends AppCompatActivity {
    private final String ACTION = "action";
    private final String UNPARK = "unpark";
    private Button unparkBtn;
    private ImageView avatar_imageView;
    private ImageView logout_btn;
    private TextView user_role;
    private UserEntity userEntity;
    private ActionService actionService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_un_parking);
        // static method
        OkHttpClient okHttpClient = Util.createOkHttpClient();
        Retrofit retrofit2 = Util.createRetrofit(okHttpClient, ACTION);
        actionService = retrofit2.create(ActionService.class);
        userEntity = (UserEntity) getIntent().getSerializableExtra(LoginActivity.USER);
        setIds();
        setInfo();
        setOnClickListeners();
    }

    private void setInfo() {
        Context context = avatar_imageView.getContext();
        int id = context.getResources().getIdentifier(userEntity.getAvatar(), "drawable", UnParkingActivity.this.getPackageName());
        avatar_imageView.setImageResource(id);
        user_role.setText(userEntity.getUsername());

    }

    private void setOnClickListeners() {
        unparkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unParkAction();
            }
        });
        logout_btn.setOnClickListener(v -> {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        });
    }

    private void unParkAction(){
        Call<Object> actionCall = actionService.invokeAction(
                new ActionEntity(null, UNPARK, new Element(new ElementId("","")),
                        null, new InvokedBy(userEntity.getUserId()), new HashMap<>()));
        actionCall.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {
                    Log.i("TAG", "onResponse: " + response.code() + "  " + "  " + response.errorBody() + "  " + response.message().getClass() + "  " + response.body().getClass());
                    Toast.makeText(UnParkingActivity.this, "Something ok: " + response.message(), Toast.LENGTH_SHORT).show();
                    Intent unParkingActivityIntent = new Intent(UnParkingActivity.this, MainActivity.class);
                    unParkingActivityIntent.putExtra(LoginActivity.USER, userEntity);
                    startActivity(unParkingActivityIntent);
                    finish();
                } else {
                    Log.i("TAG", "onResponse: " + response.code() + "  " + "  " + response.errorBody() + "  " + response.message() + "  " + response.body());
                    Toast.makeText(UnParkingActivity.this, "Something not ok: " + response.message(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Toast.makeText(UnParkingActivity.this, "Failure getAll", Toast.LENGTH_SHORT).show();
                Log.i("TAG", "onFailure: " + t);
                return;
            }
        });

    }

    private void setIds() {
        unparkBtn = findViewById(R.id.unpark_btn);
        avatar_imageView = (ImageView) findViewById(R.id.avatar_imageView);
        logout_btn = (ImageView) findViewById(R.id.logout_btn);
        user_role = (TextView) findViewById(R.id.user_name);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finishAffinity();
    }
}