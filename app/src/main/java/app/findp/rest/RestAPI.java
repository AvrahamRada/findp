//mockup class - not in use!

package app.findp.rest;
import app.findp.entities.ElementEntity;
import app.findp.entities.utils.Location;

public class RestAPI {
    public ElementEntity[] getAllElements() {

        ElementEntity[] data = new ElementEntity[11];
        Location[] testLocations = new Location[11];
        testLocations(testLocations);
        for (int i = 0; i < 11; i++) {
            ElementEntity parking = new ElementEntity("Address " + i, testLocations[i]);
            data[i] = parking;
        }
        return data;
    }

    private void testLocations(Location[] testLocations) {
        testLocations[0] = new Location(31.935091, 34.805215);
        testLocations[1] = new Location(32.184448, 34.870766);
        testLocations[2] = new Location(31.705791, 35.200657);
        testLocations[3] = new Location(31.801447, 34.643497);
        testLocations[4] = new Location(32.699635, 35.303547);
        testLocations[5] = new Location(32.017136, 34.745441);
        testLocations[6] = new Location(32.109333, 34.855499);
        testLocations[7] = new Location(32.794044, 34.989571);
        testLocations[8] = new Location(32.919945, 35.290146);
        testLocations[9] = new Location(32.166313, 34.843311);
        testLocations[10] = new Location(31.894756, 34.809322);
    }
}
