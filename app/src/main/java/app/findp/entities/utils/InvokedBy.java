package app.findp.entities.utils;

public class InvokedBy {
    private UserId userId;

    // constructor
    public InvokedBy() {
    }

    public InvokedBy(UserId userId) {
        this.userId = userId;
    }

    //Getters and Setters
    public UserId getUserId() {
        return userId;
    }

    public void setUserId(UserId userId) {
        this.userId = userId;
    }
}
