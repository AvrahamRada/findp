package app.findp.entities.utils;

import java.io.Serializable;

public class UserId implements Serializable {

    private String domain;
    private String email;

    // constructor
    public UserId(String domain, String email) {
        this.domain = domain;
        this.email = email;
    }

    public UserId(){
    }

    //Getters and Setters
    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
