package app.findp.entities.utils;

public enum UserRole {
    PLAYER, MANAGER;
}
