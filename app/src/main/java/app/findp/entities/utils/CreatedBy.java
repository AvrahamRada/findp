package app.findp.entities.utils;

import java.io.Serializable;

public class CreatedBy implements Serializable {
    private UserId userId;

    // constructor
    public CreatedBy() {
    }

    public CreatedBy(UserId userId) {
        this.userId = userId;
    }

    //Getters and Setters
    public UserId getUserId() {
        return userId;
    }

    public void setUserId(UserId userId) {
        this.userId = userId;
    }
}
