package app.findp.entities.utils;

public class ActionId {
    private String domain;
    private String id;

    // constructor
    public ActionId() {
    }

    public ActionId(String domain, String id) {
        this.domain = domain;
        this.id = id;
    }

    //Getters and Setters
    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
