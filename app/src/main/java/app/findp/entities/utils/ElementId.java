package app.findp.entities.utils;

import java.io.Serializable;

public class ElementId implements Serializable {
    private String domain;
    private String id;

    // constructor
    public ElementId() {
    }

    public ElementId(String domain, String id) {
        this.domain = domain;
        this.id = id;
    }

    //Getters and Setters
    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
