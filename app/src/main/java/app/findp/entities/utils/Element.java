package app.findp.entities.utils;

import java.io.Serializable;

public class Element implements Serializable {
    private ElementId elementId;

    // constructor
    public Element() {
    }

    public Element(ElementId elementId) {
        this.elementId = elementId;
    }

    //Getters and Setters
    public ElementId getElementId() {
        return elementId;
    }

    public void setElementId(ElementId elementId) {
        this.elementId = elementId;
    }
}
