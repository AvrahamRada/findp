package app.findp.entities.utils;

import app.findp.service.ActionService;
import app.findp.service.ElementService;
import app.findp.service.UserService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class Util {
    //const
    public static final String domain = "2020b.lior.trachtman";
    public static final int GALLERY_REQUEST = 1;
    public static final String ipAndPort = "192.168.10.76:8083";//"ipconfig:port";

    // static methods
    public static OkHttpClient createOkHttpClient(){
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build();
    }

    public static Retrofit createRetrofit(OkHttpClient okHttpClient, String type) {
        Retrofit retrofit;
        switch (type){
            case "user":
                retrofit = new Retrofit.Builder()
                        .baseUrl(UserService.BASE_URL)
                        .addConverterFactory(JacksonConverterFactory.create())
                        .client(okHttpClient)
                        .build();
                break;
            case "element":
                retrofit = new Retrofit.Builder()
                        .baseUrl(ElementService.BASE_URL)
                        .addConverterFactory(JacksonConverterFactory.create())
                        .client(okHttpClient)
                        .build();
                break;
            case "action":
                retrofit = new Retrofit.Builder()
                        .baseUrl(ActionService.BASE_URL)
                        .addConverterFactory(JacksonConverterFactory.create())
                        .client(okHttpClient)
                        .build();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }
        return retrofit;
    }
}
