package app.findp.entities;

import java.io.Serializable;

import app.findp.entities.utils.UserId;
import app.findp.entities.utils.UserRole;

public class UserEntity implements Serializable {
    private UserId userId;
    private UserRole role;
    private String username;
    private String avatar;

    // constructor
    public UserEntity(UserId userId, UserRole role, String username, String avatar) {
        this.userId = userId;
        this.role = role;
        this.username = username;
        this.avatar = avatar;
    }

    public UserEntity(){
    }

    //Getters and Setters
    public UserId getUserId() {
        return userId;
    }

    public void setUserId(UserId userId) {
        this.userId = userId;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
