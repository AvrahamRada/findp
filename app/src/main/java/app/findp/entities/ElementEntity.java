package app.findp.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import app.findp.entities.utils.CreatedBy;
import app.findp.entities.utils.ElementId;
import app.findp.entities.utils.Location;

public class ElementEntity implements Serializable {
    private ElementId elementId;
    private String type;
    private String name;
    private Boolean active;
    private Date createdTimestamp;
    private CreatedBy createdBy;
    private Location location;
    private Map<String, Object> elementAttributes;

    // constructor
    public ElementEntity(ElementId elementId, String type, String name, Boolean active, Date createdTimestamp, CreatedBy createdBy, Location location, Map<String, Object> elementAttributes) {
        this.elementId = elementId;
        this.type = type;
        this.name = name;
        this.active = active;
        this.createdTimestamp = createdTimestamp;
        this.createdBy = createdBy;
        this.location = location;
        this.elementAttributes = elementAttributes;
    }

    public ElementEntity(String name, Location location){
        this.name = name;
        this.location = location;
    }

    public ElementEntity(){
    }

    //Getters and Setters
    public ElementId getElementId() {return elementId;}

    public void setElementId(ElementId elementId) {this.elementId = elementId;}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(Date createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Map<String, Object> getElementAttributes() {
        return elementAttributes;
    }

    public void setElementAttributes(Map<String, Object> elementAttributes) {
        this.elementAttributes = elementAttributes;
    }

    @Override
    public String toString() {
        return "ElementEntity{" +
                "elementId=" + elementId +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", active=" + active +
                ", createdTimestamp=" + createdTimestamp +
                ", createdBy=" + createdBy +
                ", location=" + location +
                ", elementAttributes=" + elementAttributes +
                '}';
    }
}
